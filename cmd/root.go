/*
Copyright © 2020 m242 <m242@pm.me>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
	"os"

	"gitlab.com/m242/hush98/backend"
	"gitlab.com/m242/hush98/httpFrontend"

	"github.com/rs/zerolog"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile        string
	dbPath         string
	motd           string
	logger         zerolog.Logger
	err            error
	template       string
	templateString string
	boards         []string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "hush98",
	Short: "A textboard programm",
	Run: func(cmd *cobra.Command, args []string) {
		bckd, err := backend.Init(backend.BackendConfig{
			Logger: logger,
			DBPath: viper.GetString("dbPath"),
			MOTD:   viper.GetString("motd"),
			Boards: viper.GetStringSlice("boards"),
		})
		if err != nil {
			logger.Fatal().Err(err)
		}

		logger.Info().Msg("Backend loaded")

		// Run and serve the front end
		err = httpFrontend.Serve(bckd, httpFrontend.Config{
			Logger:   logger,
			Template: viper.GetString("templateString"),
		})
		if err != nil {
			logger.Fatal().Err(err)
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	logger = zerolog.New(os.Stderr).With().Timestamp().Logger()

	logger.Info().Msg("Starting hush98")
	if err := rootCmd.Execute(); err != nil {
		logger.Fatal().Err(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.Flags().StringVar(&cfgFile, "config", "", "config file (default is /etc/config.yaml)")
	rootCmd.Flags().StringVar(&dbPath, "database", "db", "Path to the sqlite database")
	rootCmd.Flags().StringVar(&motd, "motd", "", "Message of the day/Introduction of the textboard")
	rootCmd.Flags().StringVar(&template, "template", "tmpl", "Page template")
	rootCmd.Flags().StringVar(&templateString, "template-string", "tmplstr", "Page template as a string")
	rootCmd.Flags().StringSliceVarP(&boards, "boards", "b", boards, "List of the boards that must exist")

	viper.BindPFlag("dbPath", rootCmd.Flags().Lookup("database"))
	viper.BindPFlag("motd", rootCmd.Flags().Lookup("motd"))
	viper.BindPFlag("template", rootCmd.Flags().Lookup("template"))
	viper.BindPFlag("template", rootCmd.Flags().Lookup("template"))
	viper.BindPFlag("boards", rootCmd.Flags().Lookup("boards"))

	viper.SetDefault("dbPath", "/data/hush98.db")
	viper.SetDefault("motd", "Welcome to hush98")
	viper.SetDefault("boards", []string{"social", "bots"})
	viper.SetDefault("templateString", "<head><title>>>{{.Parent.ID}}|{{.Parent.HumanID}}</title><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><style>body{width: 60vw;margin: auto;}div.content{width: 100%;margin-left:auto;margin-right:auto;margin-top:1ch;margin-bottom: 1ch;border: solid;padding: 1%;}div.parent p{width: 80%;}div.post{border: solid;padding: 1%;width: 80%;margin: auto;}div.new-post-form{width: 80%;margin: auto;border: solid;padding: 1%;}div.new-post-form textarea{padding: 2ch;resize: vertical;width:100%;}div.new-post-form input{width:100%;}</style></head><body><div class=\"content\"><div class=\"parent\"><strong>{{.Parent.HumanID}}</strong><p>{{.Parent.Content}}</p></div>{{range .Children}}<div class=\"post\"><a href=\"{{.ID}}\">{{.ID}}</a> <strong>{{.HumanID}}</strong><br>{{.Content}}</div><br>{{end}}{{if (eq .Parent.PostType 0)}}<div class=\"new-post-form\"><form action='/{{.Parent.ID}}' method='post'><input type='text' value=\"Untitled thread\" name='humanID'><textarea id='content' name='content' contenteditable='true' rows=10></textarea><input type='submit' value='New Thread'></form></div>{{end}}{{if (eq .Parent.PostType 1)}}<div class=\"new-post-form\"><form action='/{{.Parent.ID}}' method='post'><input type='text' value=\"Anon\" name='humanID'><textarea id='content' name='content' contenteditable='true' rows=10></textarea><input type='submit' value='New Reply'></form></div>{{end}}</div></body>")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath("/config/")
		viper.AddConfigPath("./config/")
		viper.AddConfigPath(".")
		viper.SetConfigName("config")
	}

	viper.SetEnvPrefix("hush98")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
