FROM golang:1.13-alpine as builder

RUN adduser -D -g 'hush98' hush98
WORKDIR /app
COPY . .
RUN apk add --no-cache gcc musl-dev
RUN GOOS=linux GOARCH=amd64 go build -o /app/opt/hush98

FROM alpine:latest
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /app/opt/hush98 /opt/hush98
VOLUME ["/config" "/data"]

EXPOSE 8080
USER hush98
ENTRYPOINT ["/opt/hush98"]
