package backend

import (
	"github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

func (c BackendConfig) validate() error {
	return validation.ValidateStruct(
		validation.Field(&c.DBPath, validation.Required, is.URL))
}

func (p Post) validate() error {
	return validation.ValidateStruct(&p,
		validation.Field(&p.ParentID, validation.Required,
			validation.NotNil),
		validation.Field(&p.HumanID, validation.Length(1, 150),
			is.PrintableASCII),
		validation.Field(&p.Content, validation.Length(15, 1500)),
		validation.Field(&p.PostType, validation.Required,
			validation.Min(0), validation.Max(3)),
	)
}
