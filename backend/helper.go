package backend

func getPostType(postType int) string {
	switch postType {
	case 0:
		return "board"
	case 1:
		return "thread"
	case 2:
		return "message"
	default:
		return "ZBEEEEEUUUUUUUUL"
	}
}

func (b Backend) createOrUpdateMOTD(m string) error {
	var motd Post

	motd, err := b.GetPost(1)
	if err != nil {
		b.logger.Info().Msg("MOTD doesn't exist, creating")

		motd = Post{
			ParentID: 0,
			PostType: -1,
			HumanID:  "MOTD",
			Content:  m,
		}
		b.db.Create(&motd)
		return nil
	}
	if motd.Content != m {
		motd.Content = m
		b.db.Save(&motd)
	}
	return nil
}

func (b Backend) createBoardsIfNeeded(boards []string) {
	var boardPost Post
	for _, board := range boards {
		b.logger.Info().Str("board name", board).Msg("Looking for board")
		res := b.db.
			Where("human_id =? AND parent_id=1", board).First(&boardPost)
		if res.Error != nil {
			b.logger.Info().
				Str("board name", board).
				Msg("Creating Board")

			boardPost = Post{
				ParentID: 1,
				PostType: 0,
				HumanID:  board,
				Content:  "",
			}
			b.db.Create(&boardPost)
		} else {
			b.logger.Info().
				Str("board name", board).
				Msg("Found Board")
		}
	}
}
