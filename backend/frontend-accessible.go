package backend

import (
	"errors"
	"fmt"
)

type Post struct {
	ID       int `gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	ParentID int
	PostType int

	HumanID string
	Content string
}

func (b Backend) GetPost(id int) (Post, error) {
	var p Post
	b.logger.Debug().Int("ID", id).Msg("Getting Post from DB")

	res := b.db.First(&p, id)
	if res.Error != nil {
		return p, res.Error
	}

	return p, nil
}

func (b Backend) GetPostAndChildren(id int) (Post, []Post, error) {
	var (
		p        Post
		children []Post
	)
	b.logger.Debug().Int("ID", id).Msg("Getting Post and children from DB")

	p, err := b.GetPost(id)
	if err != nil {
		return p, children, err
	}

	res := b.db.Where("parent_id = ?", id).Find(&children)
	if res.Error != nil {
		return p, children, res.Error
	}
	b.logger.Warn().Int64("Number of children", res.RowsAffected)

	return p, children, nil
}

func (b Backend) NewPost(parentID int, humanID string, content string) error {
	// Get post type, and check that parent id exists
	parent, err := b.GetPost(parentID)
	if err != nil {
		return errors.New("Parent post doesn't exist !")
	}

	//content = b.sanitizer.Sanitize(content)
	//humanID = b.sanitizer.Sanitize(humanID)

	b.logger.Info().Int("ParentID", parentID).
		Str("humanID", humanID).
		Str("content", content).
		Msg("Creating new post")

	p := Post{
		ParentID: parentID,
		PostType: parent.PostType + 1,
		HumanID:  humanID,
		Content:  content,
	}
	if err := p.validate(); err != nil {
		return errors.New(fmt.Sprintf("Invalid data: '%s'", err.Error()))
	}

	if res := b.db.Create(&p); res.Error != nil {
		return res.Error
	}
	b.logger.Info().
		Int("id", p.ID).
		Str("human id", p.HumanID).
		Str("type", getPostType(p.PostType)).
		Str("content", p.Content).
		Msg("Created new record in database")

	return nil
}
