package backend

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"github.com/rs/zerolog"
)

type Backend struct {
	logger zerolog.Logger
	db     *gorm.DB
}

type BackendConfig struct {
	Logger zerolog.Logger

	// Settings
	DBPath string   // TODO allow multiple DB engines and not only sqlite
	MOTD   string   // Welcome message of the db
	Boards []string // List of the boards that must exist
}

func Init(config BackendConfig) (Backend, error) {
	if err := config.validate(); err != nil {
		config.Logger.Fatal().
			Err(err).
			Msg("Invalid configuration")
	}

	config.Logger.Debug().Msg("Connecting to the database")
	db, err := gorm.Open("sqlite3", config.DBPath)
	if err != nil {
		config.Logger.Fatal().
			Err(err).
			Str("database path", config.DBPath).
			Msg("Couldn't connect to DB")
	}
	db.AutoMigrate(&Post{})

	b := Backend{
		logger: config.Logger,
		db:     db,
		//sanitizer: bluemonday.StrictPolicy(),
	}

	b.createOrUpdateMOTD(config.MOTD)
	b.createBoardsIfNeeded(config.Boards)

	return b, nil
}
