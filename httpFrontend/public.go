package httpFrontend

import (
	"html/template"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/m242/hush98/backend"

	"github.com/rs/zerolog"
)

var (
	f frontend
)

type Config struct {
	Logger   zerolog.Logger
	Template string
}

type frontend struct {
	logger       zerolog.Logger
	bckd         backend.Backend
	routeHandler http.Handler
	tmpl         *template.Template
	port         int
}

func Serve(b backend.Backend, c Config) error {
	c.Logger.Info().Msg("Running HTTP frontend on port 8080")
	//if err := c.validate(); err != nil {
	//	c.Logger.Fatal().Err(err).Msg("Improper frontend configuration")
	//}

	c.Logger.Info().Str("path", c.Template).Msg("Loading template")
	tmpl, err := template.New("post").Parse(c.Template)
	if err != nil {
		c.Logger.Fatal().Err(err).Msg("Couldn't load templates")
	}

	f = frontend{
		logger: c.Logger,
		bckd:   b,
		tmpl:   tmpl,
	}

	f.routeHandler = createRouter(f.tmpl)

	return f.RunUntilInterruption()
}

func (f frontend) RunUntilInterruption() error {
	srv := &http.Server{
		Handler:      f.routeHandler,
		Addr:         "0.0.0.0:8080",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			f.logger.Fatal().Err(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	return nil
}
