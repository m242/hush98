package httpFrontend

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func createRouter(tmpl *template.Template) http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/1", 301)
	}).Methods("GET")
	r.HandleFunc("/{ID}", func(w http.ResponseWriter, r *http.Request) {
		// Get data
		vars := mux.Vars(r)
		id, _ := strconv.Atoi(vars["ID"])

		f.logger.Info().Int("ID", id).Msg("Serving post")
		p, children, err := f.bckd.GetPostAndChildren(id)
		if err != nil {
			f.logger.Warn().
				Err(err).
				Int("ID", id).
				Msg("Coudln't access Post")
			w.WriteHeader(http.StatusInternalServerError)
		}

		data := PageContent{
			Parent:   p,
			Children: children,
		}
		tmpl.Execute(w, data)
	}).Methods("GET")
	r.HandleFunc("/{parentID}", createPost).Methods("POST")
	return r
}

func createPost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	parentID, _ := strconv.Atoi(vars["parentID"]) // No need to error check, it's validated at post creation

	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		f.logger.Warn().Err(err).Msg("Incorrect values in creating form")
		return
	}

	humanID := r.FormValue("humanID")
	content := r.FormValue("content")

	err := f.bckd.NewPost(parentID, humanID, content)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		f.logger.Warn().Err(err).Msg("Couldn't create post in database")
		return
	}
	http.Redirect(w, r, fmt.Sprintf("/%d", parentID), http.StatusFound)
}
