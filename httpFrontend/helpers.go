package httpFrontend

import (
	"github.com/go-ozzo/ozzo-validation/v4"
)

func (c Config) validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Template, validation.Required),
	)
}
