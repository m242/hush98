package httpFrontend

import (
	"fmt"
	"gitlab.com/m242/hush98/backend"
	"io"
)

type PageContent struct {
	Parent   backend.Post
	Children []backend.Post
}

func (f frontend) formatPostAndChildren(id int, page io.Writer) error {
	p, children, err := f.bckd.GetPostAndChildren(id)
	if err != nil {
		f.logger.Warn().Err(err).Int("ID", id).Msg("Coudln't get Post")
		fmt.Fprint(page, "No post here")
		return nil
	}

	data := PageContent{
		Parent:   p,
		Children: children,
	}
	return f.tmpl.Execute(page, data)
}
