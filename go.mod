module gitlab.com/m242/hush98

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/go-ozzo/ozzo-validation/v4 v4.2.2
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/microcosm-cc/bluemonday v1.0.4
	github.com/rs/zerolog v1.19.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
)
